package pl.edu.pwsztar.Millionaire.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.Millionaire.dao.QuestionData;
import pl.edu.pwsztar.Millionaire.model.Question;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;


@Service
public class QuestionService {
    private  final QuestionData questionData;

    @Autowired
    public QuestionService(QuestionData questionData) {
        this.questionData = questionData;
    }

    public Question LoadQuestion(int id){
        try {
            Random random = new Random();
            BufferedReader br = new BufferedReader((new FileReader("bazy/" + id + ".csv")));
            String line;

            int rnd = random.nextInt(10)+1;

            // Zignoruj pierwsza linie
            line=br.readLine();
            line=null;

            while ((line=br.readLine())!=null)
            {
                String [] data = line.split(";");
                int qId = Integer.parseInt(data[0]);
                if (qId == rnd)
                {

                    String [] ans = {data[2],data[3],data[4],data[5]};
                    int correct = Integer.parseInt(data[6]);
                    Question q = new Question(qId,data[1],ans,correct);
                    br.close();
                    return questionData.SendQuestion(q);
                }
            }
        }catch (IOException e){
            return null;
        }

        return null;
    }
}
