package pl.edu.pwsztar.Millionaire.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwsztar.Millionaire.model.Question;
import pl.edu.pwsztar.Millionaire.service.QuestionService;

@RequestMapping("millionaire/question")
@RestController
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping(path = "{id}")
    public Question getQuestionById(@PathVariable("id") int id){
        return questionService.LoadQuestion(id);
    }
}
