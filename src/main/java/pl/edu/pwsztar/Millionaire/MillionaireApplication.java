package pl.edu.pwsztar.Millionaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MillionaireApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(MillionaireApplication.class, args);
	}

}
